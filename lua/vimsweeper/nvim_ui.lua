local engine = require"vimsweeper.engine"

local win, buf
local uisettings = {}
uisettings.sign_closed = "abcdefghijklmnopqrstuvwxyz"
uisettings.sign_bomb = "@"
uisettings.sign_sep = " "

local function set_mappings()
    local mappings = {
        ['<C-q>'] = 'close()',
        ['<C-r>'] = 'start_new_game()',
        ['<space>'] = 'ackmove()'
    }

    for k,v in pairs(mappings) do
        vim.api.nvim_buf_set_keymap(buf, 'n', k, ':lua require"vimsweeper".'..v..'<cr>', {
            nowait = true, noremap = true, silent = true
        })
    end
end

local function create_win()
    start_win = vim.api.nvim_get_current_win()

    vim.api.nvim_command('new')
    win = vim.api.nvim_get_current_win()
    buf = vim.api.nvim_get_current_buf()

    vim.api.nvim_buf_set_name(buf, 'Vimsweeper #' .. buf)

    -- nofile prevent mark buffer as modified so we never get warnings about not saved changes.
    vim.api.nvim_buf_set_option(buf, 'buftype', 'nofile')
    vim.api.nvim_buf_set_option(buf, 'swapfile', false)
    -- This allows to create own autocommand or colorschemes on filetype.
    vim.api.nvim_buf_set_option(buf, 'filetype', 'vimsweeper')

    vim.api.nvim_win_set_option(win, 'wrap', false)
    vim.api.nvim_win_set_option(win, 'cursorline', true)

    set_mappings()
end

local function close()
    if win and vim.api.nvim_win_is_valid(win) then
        vim.api.nvim_win_close(win, true)
    end
end

local function click_field()
    if win and vim.api.nvim_win_is_valid(win) then
        local row, col = unpack(vim.api.nvim_win_get_cursor(win))
        if col%2 == 1 then
            return row, math.floor(col/2)+1
        else
            return -1, -1
        end
    end
end

local function draw()
    -- First we allow introduce new changes to buffer. We will block that at end.
    vim.api.nvim_buf_set_option(buf, 'modifiable', true)

    local list = {}

    -- Now we populate our list with X last items form oldfiles
    local len_sign_closed = string.len(uisettings.sign_closed)
    for i=1,engine.numrows do
        local line=""
        for j=1,engine.numcols do
            if engine.boardopen[i][j] then
                if engine.board[i][j] == -1 then 
                    line=line..uisettings.sign_sep..uisettings.sign_bomb
                else
                    line=line..uisettings.sign_sep..engine.board[i][j]
                end
            else
                local stridx = (j-1)%len_sign_closed+1
                line=line..uisettings.sign_sep..string.sub(uisettings.sign_closed, stridx, stridx)
            end
        end
        table.insert(list, #list + 1, line)
    end

    -- We apply results to buffer
    vim.api.nvim_buf_set_lines(buf, 0, -1, false, list)
    -- And turn off editing
    vim.api.nvim_buf_set_option(buf, 'modifiable', false)
end

local function print_lost()
    vim.api.nvim_buf_set_option(buf, 'modifiable', true)
    vim.api.nvim_buf_set_lines(buf, -1, -1, false, {"YOU LOST!!!"})
    vim.api.nvim_buf_set_option(buf, 'modifiable', false)
end

local function print_win()
    vim.api.nvim_buf_set_option(buf, 'modifiable', true)
    vim.api.nvim_buf_set_lines(buf, -1, -1, false, {"YOU WIN!!!"})
    vim.api.nvim_buf_set_option(buf, 'modifiable', false)
end

return {
    create_win = create_win,
    draw = draw,
    set_mappings = set_mappings,
    close = close,
    click_field = click_field,
    print_lost = print_lost,
    print_win = print_win
}

