local M={}
local settingsGame={}

-- M = require"nvim_ui"
engine = require"vimsweeper.engine"
ui = require"vimsweeper.nvim_ui"
local gamestate

local function print_endmessage(gamestate)
    if gamestate == -1 then
        ui.print_lost()
    elseif gamestate == 1 then
        ui.print_win()
    end
end

local function start_new_game()
    ui.close()
    gamestate = 0
    settingsGame.numrows = 9
    settingsGame.numcols = 9
    settingsGame.numbombs = 10

    engine.init_game(settingsGame.numrows, settingsGame.numcols, settingsGame.numbombs) 
    ui.create_win()
    ui.draw()
end

local function ackmove()
    if gamestate==-1 or gamestate==1 then
        start_new_game()
    end
    local row, col = ui.click_field()
    if row==-1 then
        return
    end
    gamestate = engine.makemove(row, col)
    if gamestate==0 then
        ui.draw()
    else
        engine.open_all_fields()
        ui.draw()
        print_endmessage(gamestate)
        -- todo: restart game with previous settings on calling ackmove()
    end
end

start_new_game()

M.close = ui.close
M.ackmove = ackmove
M.start_new_game = start_new_game
return M
