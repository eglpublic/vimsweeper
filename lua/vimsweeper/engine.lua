#!/usr/bin/env lua
local engine = {}

-- board is a matrix 
-- values >= 0 are the number of neighbour bombs
-- value ==-1 stands for a bomb
engine.board = {}
engine.boardopen = {}
engine.boardsize = nil
engine.numrows = nil
engine.numcols = nil
engine.numbombs = nil
engine.wincount = nil
engine.movecount = nil

function engine.init_game(numrows, numcols, numbombs)
    -- dont throw errors just overwrite insane arguments
    if numrows < 2 then numrows=2 end
    if numcols < 2 then numcols=2 end
    engine.numrows = numrows
    engine.numcols = numcols
    engine.boardsize = engine.numrows*engine.numcols
    if numbombs-1 >= engine.boardsize then numbombs=engine.boardsize-1 end
    engine.numbombs = numbombs
    engine.wincount = engine.boardsize - engine.numbombs
    engine.movecount = 0
    -- first we will init matrix without bombs
    -- and then will set bombs randomly and update neighbourbomb count
    for i=1,engine.numrows do
        engine.board[i]={}
        engine.boardopen[i]={}
        for j=1,engine.numcols do
            engine.board[i][j]=0
            engine.boardopen[i][j]=false
        end
    end
end

function engine.init_board(saferow, safecol)
    engine.set_bombs_random(saferow, safecol)
end

function engine.set_bombs_random(saferow, safecol)
    -- set bombs in matrix
    -- do not set bombs into fields with safeidx
    math.randomseed(os.time())
    local safeidx={}
    safeidx[1] = (saferow-1)*engine.numcols + safecol
    local safecount = 1
    for i=1,engine.numbombs do
        local insertidx = 0
        local bombidx = math.random(engine.boardsize-safecount)
        -- debug print
        -- print(engine.boardsize, safecount, bombidx)
        for safeidx_i, safeidx_v in pairs(safeidx) do
            if bombidx < safeidx_v then 
                insertidx = safeidx_i
                break
            else
                bombidx = bombidx+1
            end
        end
        if insertidx == 0 then 
            table.insert(safeidx, bombidx) 
        else 
            table.insert(safeidx, insertidx, bombidx)
        end
        safecount = safecount+1
        -- // operator will only work in version >= 5.3
        -- engine.set_bomb((bombidx-1)//engine.numcols+1, (bombidx-1)%engine.numcols+1)
        engine.set_bomb(math.floor((bombidx-1)/engine.numcols+1), (bombidx-1)%engine.numcols+1)
        -- debug
        -- engine.debug_board()
    end

end

function engine.set_bomb(row, col)
    engine.board[row][col] = -1
    for i=row-1,row+1 do
        if i>0 and i<=engine.numrows then
            for j=col-1,col+1 do
                if j>0 and j<=engine.numcols and engine.board[i][j]~=-1 then
                    engine.board[i][j] = engine.board[i][j]+1
                end
            end
        end
    end
end

function engine.open_field(row, col)
    -- open recursively for fields with no bomb-neighbour
    -- update boardopen
    -- return the game-state 
    --      -1 -> lost
    --       0 -> game running
    --       1 -> won
    if row<1 or row>engine.numrows then
        -- print("DBG: out of range")
        return 0
    elseif col<1 or col>engine.numcols then
        -- print("DBG: out of range")
        return 0
    end
    -- print("DBG: fieldstate=", engine.board[row][col])
    if engine.boardopen[row][col] then
        -- print("DBG: already open")
        return 0
    elseif engine.board[row][col]<0 then
        -- print("DBG: hit bomb")
        engine.boardopen[row][col] = true
        return -1
    elseif engine.board[row][col]>0 then
        -- print("DBG: hit neighbour")
        engine.boardopen[row][col] = true
        engine.wincount = engine.wincount - 1
        if engine.wincount < 1 then
            return 1
        else
            return 0
        end
    else
        -- print("DBG: hit null")
        engine.boardopen[row][col] = true
        engine.wincount = engine.wincount - 1
        if engine.wincount < 1 then
            return 1
        end
        for i=row-1,row+1 do
            for j=col-1,col+1 do
                -- print("DBG: recurse i=", i, " j=", j)
                retval = engine.open_field(i, j)
                -- print("DBG: retval = ", retval)
                if retval==1 or retval==-1 then
                    return retval
                end
            end
        end
        return 0
    end
end

function engine.open_all_fields()
    for i=1,engine.numrows do
        for j=1,engine.numcols do
            engine.boardopen[i][j]=true
        end
    end
end

function engine.makemove(row, col)
    if engine.movecount == 0 then
        engine.init_board(row, col)
    end
    engine.movecount = engine.movecount+1 
    return engine.open_field(row, col)
end

function engine.print_board()
    local symbols_closed = "abcdefghijklmnopqrstuvwxyz"
    local len_symbols_closed = string.len(symbols_closed)
    local symbol_bomb = "@"
    local symbol_sep = " "
    for i=1,engine.numrows do
        local line=""
        for j=1,engine.numcols do
            if engine.boardopen[i][j] then
                if engine.board[i][j] == -1 then 
                    line=line..symbol_sep..symbol_bomb
                else
                    line=line..symbol_sep..engine.board[i][j]
                end
            else
                local stridx = (j-1)%len_symbols_closed+1
                line=line..symbol_sep..string.sub(symbols_closed, stridx, stridx)
            end
        end
        print(line)
    end
end

function engine.debug_board()
    for i=1,engine.numrows do
        local line=""
        for j=1,engine.numcols do
            if engine.board[i][j] == -1 then 
                line=line.." x"
            else
                line=line.." "..engine.board[i][j]
            end
        end
        print(line)
    end
end

function engine.debug_boardopen()
    for i=1,engine.numrows do
        local line=""
        for j=1,engine.numcols do
            if engine.boardopen[i][j] then 
                line=line.." 1"
            else
                line=line.." 0"
            end
        end
        print(line)
    end
end

return engine
